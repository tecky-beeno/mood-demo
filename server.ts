import express from 'express';
import {Client} from 'pg';

let app = express();
let port = 8100;

let client = new Client({
    database: '',
    user: '',
    password: '',
});
client.connect();

app.get('/recommend-combinations', async (req, res) => {
    if ('hardcode') {
        res.json({
            mood: req.query.mood,
            combinations: [
                {id: 1, name: 'japanses trendy', image: 'japan-1.png'},
                {id: 2, name: 'toyko trendy', image: 'japan-2.png'},
                {id: 3, name: 'osaka trendy', image: 'japan-3.png'},
            ],
        });
    }
    try {
        let result = await client.query(
            `
    select
      combinations.id,
      combinations.name,
      combinations.image
    from combinations
    where mood = $1`,
            [req.query.mood],
        );
        res.json({
            mood: req.query.mood,
            combinations: result.rows,
        });
    } catch (e) {
        res.status(500).json({error: e.toString()});
    }
});

app.use(express.static('public'));
app.use(express.static('images'));

app.listen(port, () => {
    console.log('listening on http://localhost:' + port);
});
